<?php

include_once("Utility/database.php");
include_once("Utility/session.php");
include_once("Utility/cookie.php");
use CORE\UTILITY;

UTILITY\Session::init();

$data['session_id'] = UTILITY\Session::get("session_id");
$data['token'] = UTILITY\Session::get("_token");
$table = "session";
$condition = array("=", "AND", "=");

$database = UTILITY\Database::getInstance();

$result = $database->delete($table, $data, $condition);

$dacancellare = $data['session_id'];
UTILITY\Cookie::delete($dacancellare);

UTILITY\Cookie::delete("_token");
UTILITY\Session::destroy();


header('location:../Index.php');

?>