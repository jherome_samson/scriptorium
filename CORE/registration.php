<?php
 
require_once('Utility/database.php');
require_once("Index.php");
use CORE\Index;
$control = new Index\controller;


if (isset($_POST['name']) && 
    isset($_POST['lastname']) &&
    isset($_POST['email']) &&
    isset($_POST['username']) &&
    isset($_POST['password']) &&
    isset($_POST['day_birth']) &&
    isset($_POST['month_birth']) &&
    isset($_POST['year_birth']) &&
    isset($_POST['gender']))
    {
        $data = [];

        $data["name"] = $_POST['name'];
        $data["lastname"] = $_POST['lastname'];
        $data["email"] = $_POST['email'];
        $data["username"] = $_POST['username'];
        $data["password"] = $_POST['password'];
        $day_birth = $_POST['day_birth'];
        $month_birth = $_POST['month_birth'];
        $year_birth = $_POST['year_birth'];
        $data["birthday"] = $year_birth . $month_birth . $day_birth;
        $data["gender"] = $_POST['gender'];

        if (isset($_POST['personaldescription'])){
            $data["pdesc"] = $_POST['personaldescription'];
        }
        
        $userRegistration = new Database();
        $result = $userRegistration->insert('user', $data);
    }
    
   /* if ($result){
     header('Location: ../SERVERSIDE/home.php');
    }
    else echo "non sei registrato"; */
?>
