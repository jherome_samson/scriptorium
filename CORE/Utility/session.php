<?php

namespace CORE\UTILITY;

class Session {

    public static function destroy() {

        session_destroy();
    }

    public static function exists($key_ID) {

        return(isset($_SESSION[$key_ID]));
    }

    public static function get($key) {

        if (self::exists($key)) {

            return($_SESSION[$key]);
        }
    }

    public static function init() {

        if (session_id() == "") {

            $r =session_id();
            echo $r;
            session_start();
        
            /*if (isset($_COOKIE['PHPSESSID'])) {

                session_regenerate_id();
                }*/
        } 
    }

    public static function put($key, $value) {

        return($_SESSION[$key] = $value);
    }

    /*public static function regenerate() {

        if (isset(session_id())) {
            session_regenerate_id();
        }
    }*/


}

?>