<?php

namespace CORE\UTILITY;

class Database{

    private $_database = NULL;
    
    private $_query = NULL;

    private $_result = [];

    private $_error = NULL;

    protected $_connection = NULL;


    public function __construct(){

        $this->connection();

    }


    private function connection(){
        
        try{

            static $host = "localhost";

            static $user = "root";

            static $psw = "";

            static $database = "test";

            $this->_connection = new \mysqli($host, $user, $psw, $database);

           /* if($this->_connection)
                {echo "sei connesso";}
                
                else
                    {echo "non sei connesso";}*/

        }catch (Exception $e){

            die("connect: %s\n" . $this->_connection-> error);
            
        }        
    }


    public static function getInstance() {

        if (!isset($_database)) {
            $_database = new Database();
        }
        return($_database);
    }


    public function insert($table, array $array){

        $params = [];
        $str_column = [];
        $str_values = [];

        foreach($array as $key => $value){

            $params[] = $key; 
        }
        $str_values = implode("', '", $array);
        $str_column = implode(", ", $params);


        $_query = "INSERT INTO $table ( $str_column ) VALUES ( '$str_values' );";
        $_result = $this->_connection->query($_query);
        
        return $_result;
    }


    public function select($field, $table, array $array, array $condition) {
        
        $str_condition = NULL;
        $i = 0;

        foreach ($array as $key => $value) {
            
            $str_condition .= $key . " " . $condition[$i] . " " . "'".$value."'";

                if(isset($condition[$i+1])) {
                
                $i++;
                $str_condition .= " " . $condition[$i] . " ";
                $i++;
            }
        }

        $_query = "SELECT " . $field . " FROM " . $table . " WHERE " .$str_condition;
        $_result = $this->_connection->query($_query);

        $result = $_result->fetch_array(MYSQLI_ASSOC);
        /*echo "<pre>";
        print_r($result);
        echo "</pre>";*/
        return $result;
    }


    public function delete($table, array $data, array $condition) {

        $str_condition = "DELETE FROM " . $table . " WHERE " ;
        $i = 0;

        foreach ($data as $key=>$value) {

            $str_condition .= $key . " " . $condition[$i] . " " . "'" . $value . "'";

            if (isset($condition[$i+1])){

                $i++;
                $str_condition .= " " . $condition[$i] . " ";
                $i++; 

            }
        }

        $this->_query = $str_condition;
        $this->_result = $this->_connection->query($this->_query);

        echo $this->_query;

        echo "<pre>";
        print_r($this->_result);
        echo "</pre>";
        
      //  $result = $this->_result->fetch_array(MYSQLI_ASSOC);

        //return $result;

    }

}
    
?>