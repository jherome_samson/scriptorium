<?php

namespace CORE\UTILITY;

include_once("token.php");
include_once("database.php");

use CORE\UTILITY;

class Authorization {

    public static function checkAuthenticated() {   
        
        $field = "ID_user";

        $table = "session";

        $data['session_id'] = $_COOKIE['PHPSESSID'];
        
        $data['token'] = $_COOKIE['_token'];

        $condition = array("=", "AND", "=");

        $database = new UTILITY\Database;

        $result = $database->select($field, $table, $data, $condition);

        if (isset($result)) {

            return  $result;
        }

    }

    public static function generateToken(){

        $token = UTILITY\Token::newToken();

        return $token;
    }



    

}

?>