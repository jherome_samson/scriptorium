<?php

use CORE\UTILITY;


require_once("Utility/database.php");
require_once("Utility/session.php");
require_once("Utility/auth.php");
require_once("Utility/cookie.php");

UTILITY\Session::init();

if (isset($_POST['email_login']) &&
    isset($_POST['password_login']))
{
    $data['email'] = $_POST['email_login'];
    $data['password'] = $_POST['password_login'];
    $condition = array("=", "AND", "=");
    $field = 'ID';
    $table = 'user';

    $data['email'] = filter_var($data['email'], FILTER_SANITIZE_EMAIL);
    
    $Database = UTILITY\Database::getInstance();
    $result = $Database->select($field, $table, $data, $condition);

    if (isset($result)){

        $auth = new UTILITY\Authorization;
        $token = $auth::generateToken($result);

        $session_id = $_COOKIE['PHPSESSID'];
        UTILITY\Session::put('session_id', $session_id );
        UTILITY\Session::put("_token", $token);
        UTILITY\Cookie::put("_token", $token, 3600);

        $datasession['session_id'] = $_COOKIE['PHPSESSID'];
        $datasession['token'] = $token;
        $datasession['ID_user'] = $result['ID'];

        $database = new UTILITY\Database;

        $database->insert("session", $datasession );        
        
        header('location: ../Index.php');
    } else 
        echo "non ti sei connesso";

}
?>