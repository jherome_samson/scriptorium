<?php

namespace CORE\VIEW;

use CORE\Utility;

class View {


    public function addheader() {

        include_once(WEBSITE_INCLUDES . "header.php");

        if (isset($_COOKIE['_token']))
            include_once(WEBSITE_INCLUDES . "logged.php");
        else
            include_once(WEBSITE_INCLUDES . "login.php");

    }

    public function addfooter() {

        include_once(WEBSITE_INCLUDES . "footer.html");
        }
    
    public function addbody($body) {
            
        if($body) 
            include_once(ROOT .  "/www/template/" . $body . ".php");
            else 
                include_once(ROOT . "/www/template/home.php");
    }            

    public function makeURL($URL) {

        if ($URL) {
            $path_URL = "CORE/" . $URL . ".php";
        }
        return $path_URL;
    }

    public function goto($page) {

        if ($page){
            //header('location: ' . ROOT . 'Index.php' . '?pg=' .$page);
            //header('location: Index.php' . '?pg=' .$page);
            echo "http://localhost/Scriptorium/Index.php?pg=" . $page;
        }
    }

    public function getCSS($CSS) {

        if ($CSS) {
            //$path_CSS = WEBSITE_CSS . $CSS . ".css";
            $path_CSS = "www/template/css/" . $CSS . ".css";
        }
        return $path_CSS;
    }

    public function getIMG($IMG) {

        if ($IMG) {
            //$path_IMG = WEBSITE_IMG . $IMG . ".png";
            $path_IMG = "www/template/img/" . $IMG . ".png";
        }
        return $path_IMG;
    }

    public function getJS($JS) {

        if ($JS) {
            //$path_JS = WEBSITE_JS . $JS . ".js";
            $path_JS = "www/template/js/" . $JS . ".js";
        }
        return $path_JS;
    }

    public function run() {

        $this->addbody();
    }

}

?>