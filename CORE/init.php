<?php

include_once("VIEW/View.php");
include_once("Utility/cookie.php");
include_once("Utility/session.php");


//ROOT
define("ROOT", realpath(dirname(__FILE__). "/../") . "/");  //realpath() -> toglie il non essenziale e mette a disposizione il canonico 

//APP CONFIGURATION
define("WEBSITE_NAME", "Scriptorium");
define("WEBSITE_CORE", ROOT . "CORE/");                  //aggiunge alla ROOT ...../app/
define("APP_PROTOCOL", stripos($_SERVER["SERVER_PROTOCOL"], "https") === true ? "https://" : "http://");   //PROTOCOLLO HTTP, HTTPS $_SERVER["SERVER_PROTOCOL"]->stampa HTTP o HTTPS se uguale https=APP_PROTOCOL=https se FALSE APP_PROTOCOL=http
define("APP_URL", APP_PROTOCOL . $_SERVER["HTTP_HOST"] . str_replace("Public", "Public", dirname($_SERVER["SCRIPT_NAME"])) . "/");
define("APP_CONFIG_FILE", WEBSITE_CORE . "config.php");

// template path
define("WEBSITE_TEMPLATE", "www/template/");
define("WEBSITE_CSS",ROOT . WEBSITE_TEMPLATE . "css/");
define("WEBSITE_IMG",ROOT . WEBSITE_TEMPLATE . "img/");
define("WEBSITE_JS", ROOT . WEBSITE_TEMPLATE . "js/");
define("WEBSITE_INCLUDES", ROOT . WEBSITE_TEMPLATE . "includes/");


//CONTROLLER CONFIG
//define("CONTROLLER_PATH");
/*
echo ROOT . "<-ROOT" . "<br>" . "<br>" ; 
echo WEBSITE_NAME . "<-WEBSITE_NAME" . "<br>" . "<br>";
echo WEBSITE_ROOT . "<-WEBSITE_ROOT" .  "<br>". "<br>";
echo APP_PROTOCOL . "<-APP_PROTOCOL" .  "<br>". "<br>";
echo APP_URL . "<-APP_URL" .  "<br>". "<br>";
echo APP_CONFIG_FILE . "<-APP_CONFIG_FILE" .  "<br>" . "<br>";
echo $_SERVER["SCRIPT_NAME"];
*/


?>