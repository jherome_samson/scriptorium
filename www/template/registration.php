     <div class="access_content">
               <div class="area_A clearfix">
                 <div class="area_A1">
                    <div class="area_title_">
                         <div class="title_div"><h2>Crea un nuovo account</h2></div>
                    </div>
                    <div class="area_A2_form_">     
                         <div class="form_registration">
                              <form action="../SERVERSIDE/sign_up.php" method="post">
                                <div class="st_part">
                                     <div class="data_1">
                                          <div class="name">
                                               <input type="text" name="name" id="id_name" required>
                                               <label for="name">Name</label> 
                                            <span class="focus-border"></span>
                                          </div>
                                          <div class="lastname">
                                               <input type="text" name="lastname" id="id_lastname" required>
                                               <label for="lastname">Last Name</label> 
                                            <span class="focus-border"></span>
                                          </div>
                                     
                                          
                                     <div  class="class_email">                  
                                          <input type="text" name="email" id="id_email" required>
                                          <label for="Email">E-mail</label>
                                          <span class="focus-border"></span>
                                     </div>

                                     <div  class="class_username">
                                          <input type="text" name="username" id="id_username" required>
                                          <label for="username">Username</label>
                                          <span class="focus-border"></span>
                                     </div>
                                     <div   class="class_password">
                                          <input type="password" name="password" id="password" required>
                                          <label for="password">Password</label>
                                          <span class="focus-border"></span>
                                     </div>
                                   </div>
                                </div>

                                <div class="nd_part">   
                                   <div class="data_2">
                                     <div id="id_birthday" class="class_birthday">
                                          <label for="birthday">Birthday</label>
                                        <div id="birthday">
                                          <input type="number" min="1" max="31" name="day_birth" id="day_birth" >
                                          <input type="number" min="1" max="12" name="month_birth" id="month_birth" >
                                          <input type="number" min="1930" max="2019" name="year_birth" id="year_birth" >
                                        </div>
                                     </div>
                                     <div id="id_gender" class="class_gender">
                                          <label for="male">Male</label>
                                          <input type="radio" name="gender" id="male" value="male">
                                          <label for="female">Female</label>
                                          <input type="radio" name="gender" id="female" value="female">
                                     </div>
                                   </div> 
                                 </div> 

                                 <div class="rd_part">
                                      <div class="data_3">
                                           <div id="id_description" class="class_description">
                                                <input type="text" name="personaldescription" id="personaldescription">
                                                <label for="description">Personal Description</label>
                                                <span class="focus-border"></span>
                                           </div>
                                      </div>
                                 </div>

                              <div class="rd_part_submit_btn">
                                   <div id="submit" class="class_submit">
                                        <input type="submit" value="Sign In">
                                   </div>
                              </div>
                              
                            </form>
                         </div>
                    </div>
                    
                 </div>
               </div>
           
               
                    
     </div>




<!--

<div class="mahi_holder">
          <div class="container">
            
            <div class="row bg_3">
              <h2><i>Input with Label Effects</i></h2>
              <div class="col-3 input-effect">
                   <input class="effect-16" type="text" placeholder="">
                  <label>First Name</label>
                  <span class="focus-border"></span>
              </div>
              <div class="col-3 input-effect">
                   <input class="effect-17" type="text" placeholder="">
                  <label>First Name</label>
                  <span class="focus-border"></span>
              </div>
              <div class="col-3 input-effect">
                   <input class="effect-18" type="text" placeholder="">
                  <label>First Name</label>
                  <span class="focus-border"></span>
              </div>
              <div class="col-3 input-effect">
                   <input class="effect-19" type="text" placeholder="">
                  <label>First Name</label>
                  <span class="focus-border">
                       <i></i>
                  </span>
              </div>
              <div class="col-3 input-effect">
                   <input class="effect-20" type="text" placeholder="">
                  <label>First Name</label>
                  <span class="focus-border">
                       <i></i>
                  </span>
              </div>
              <div class="col-3 input-effect">
                   <input class="effect-21" type="text" placeholder="">
                  <label>First Name</label>
                  <span class="focus-border">
                       <i></i>
                  </span>
              </div>
              <div class="col-3 input-effect">
                   <input class="effect-22" type="text" placeholder="">
                  <label>First Name</label>
                  <span class="focus-bg"></span>
              </div>
              <div class="col-3 input-effect">
                   <input class="effect-23" type="text" placeholder="">
                  <label>First Name</label>
                  <span class="focus-bg"></span>
              </div>
              <div class="col-3 input-effect">
                   <input class="effect-24" type="text" placeholder="">
                  <label>First Name</label>
                  <span class="focus-bg"></span>
              </div>
            </div>
          </div>
      </div>
      












!-->





