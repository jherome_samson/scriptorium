<!DOCTYPE html>
<html lang="it">
 <head>
   <link rel="stylesheet" type="text/css" href="<?= $this->getCSS("header"); ?>"/> 
   <script type="text/javascript" src="<?= $this->getJS("login"); ?>"></script>
 </head>
  <body>
     <div class="global_container">
     <div class="container">       
          <header class="header clearfix">
          <div class="header_dimension">         
            <a class="header_logo" href=""> <img src="<?= $this->getIMG("logo"); ?>" alt="LOGO">   
            <h1 class="header_logo_">Scriptorium</h1> </a>
          </div>
          
          <div class="bar">
            <div class= "link">
            <a class="active" href="<?= $this->goto("home");?>">Home</a>
            <a href="<?= $this->goto("contact");?>"> Contact</a>
            <a href="<?= $this->goto("about");?>">About</a>
          </div>