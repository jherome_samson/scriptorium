<?php
namespace CORE\Index;
include_once("CORE/init.php");
include_once("CORE/Utility/auth.php");
include_once("CORE/Utility/session.php");

use CORE\VIEW;
use CORE\UTILITY;

$session = new UTILITY\Session;

UTILITY\Session::init();

if(isset($_COOKIE['_token'])){

    UTILITY\Authorization::checkAuthenticated();
}

        $view = new VIEW\View;

        $view->addheader();
        
        if (isset($_GET['pg']))
            $view->addbody($_GET['pg']);
        else
            $view->addbody("home");

        $view->addfooter();



/*
extends Database{

    public function __construct(){

    }

    public function dataController(array $data) {

        $conn = new Database;

        foreach ($data as $key => $value) {

            $dataControlled[$key] = mysqli_real_escape_string($conn->_connection, $value);
        }
        return $dataControlled;
    }

    public function controlLogin() {

            $data['email'] = filter_var($data['email'], FILTER_SANITIZE_EMAIL);
            $data['password'] = filter__var($data['password'], FILTER_SANITIZE);    


    }

}

*/

?>